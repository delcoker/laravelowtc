/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function footer_info() {
    $(".page-footer").html('<div class="footer-copyright">Made by <a class="blue-grey-text text-lighten-3" href="http://www.twitter.com/niiapa">blankieGH</a>'
            + ' & <a class="blue-grey-text text-lighten-3" href="http://www.twitter.com/niiapa">deLoop</a></div>');
}
footer_info();


function callAjaxSuccessful(data) {
//    prompt("successful ajax call ", data);
}

function errorFunction() {
    alert("ajax function failed");
}

function syncAjax(u, arr) {
//    alert(arr[0]);
    var obj = $.ajax(online_url + u, {async: false
        , type: 'GET'
        , data: arr // {cmd:3} //JSON.stringify(arr)     //  {cmd:3}// ?cmd=3
//        , dataType: String
        , success: callAjaxSuccessful   //            function(data){alert(data);}
        , error: errorFunction});
    return $.parseJSON(obj.responseText);
}


initializeDatePickers();

function initializeDatePickers() {
    var from_input = $('#check-in').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 4 // Creates a dropdown of 15 years to control year
        , formatSubmit: 'yyyy-mm-dd'
//            , format: 'yyyy-mm-dd'
        , hiddenSuffix: '_hidden'
        , hiddenName: true
        , min: true //Limits to today as minimum
    });
    var from_picker = from_input.pickadate('picker');

    var to_input = $('#check-out').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 4 // Creates a dropdown of 15 years to control year
        , formatSubmit: 'yyyy-mm-dd'
//            , format: 'yyyy-mm-dd'
        , hiddenSuffix: '_hidden'
        , hiddenName: true
        , min: true //Limits to today as minimum
    });
    var to_picker = to_input.pickadate('picker');

    // Check if there’s a “from” or “to” date to start with.
    if (from_picker.get('value')) {
        to_picker.set('min', from_picker.get('select'));
    }
    if (to_picker.get('value')) {
        from_picker.set('max', to_picker.get('select'));
    }

    // When something is selected, update the “from” and “to” limits.
    from_picker.on('set', function (event) {
        if (event.select) {
            to_picker.set('min', from_picker.get('select'));
        } else if ('clear' in event) {
            to_picker.set('min', false);
        }
    });
    to_picker.on('set', function (event) {
        if (event.select) {
            from_picker.set('max', to_picker.get('select'));
        } else if ('clear' in event) {
            from_picker.set('max', false);
        }
    });
}