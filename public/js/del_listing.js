/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var online_url = "";


//sources: http://stackoverflow.com/questions/404891/how-to-pass-values-from-one-page-to-another-in-jquery

function qs() {
    var qsParm = new Array();
    var query = window.location.search.substring(1);
    var parms = query.split('&');
    for (var i = 0; i < parms.length; i++) {
        var pos = parms[i].indexOf('=');
        if (pos > 0) {
            var key = parms[i].substring(0, pos);
            var val = parms[i].substring(pos + 1);
            // decoded uri from address bar
            qsParm[key] = decodeURIComponent(val);
        }
    }
    return qsParm;
}
//sources: http://stackoverflow.com/questions/404891/how-to-pass-values-from-one-page-to-another-in-jquery
//////////////////////////////////////////////////////////////////////////////////////////////

function syncAjax(u, arr) {
//    alert(arr[0]);
    var obj = $.ajax(online_url + u, {async: false
        , type: 'GET'
        , data: arr // {cmd:3} //JSON.stringify(arr)     //  {cmd:3}// ?cmd=3
//        , dataType: String
        , success: callAjaxSuccessful   //            function(data){alert(data);}
        , error: errorFunction});
    return $.parseJSON(obj.responseText);
}

$(document).ready(function () {

    var details = qs();

    // parse picture to JSON
    var picts = $.parseJSON(details.pictures);

    // javasipt unable to do this  -> does sth wierd
    var picUrl = picts[0].uri;
//    picUrl = picUrl.replace().replace(/"/g, "");
//    debugger
    if (picUrl === undefined) {
        picUrl = "office.jpg";
    }

    var listing_page_header = "";
    listing_page_header += '<div id="listing-page-header" class="card">';
    listing_page_header += '<div class="card-image waves-effect waves-block waves-light">';
    listing_page_header += '<img class="activator" src="images/bg.jpg" alt="user background">';
    listing_page_header += '  </div>';
    listing_page_header += ' <div class="card-profile-image">';
    listing_page_header += '     <img src="uploads/' + picUrl + '" alt="profile image" class="circle responsive-img activator">';
    listing_page_header += ' </div>';
    listing_page_header += '  <div class="card-content">';
    listing_page_header += '      <div class="row">';
    listing_page_header += '         <div class="col s12 m2 offset-m2 l2 offset-l2 center-align">';
    listing_page_header += '             <h4 class="card-title grey-text text-darken-4">' + details.name + '</h4>';
    listing_page_header += '            <p class="medium-small grey-text">Host</p>';
    listing_page_header += '       </div>';
    listing_page_header += '      <div class="col s6 m2 l2 center-align">';
    listing_page_header += '           <h4 class="card-title grey-text text-darken-4">' + details.room_type.replace(/\+/g, " ") + '</h4>';
    listing_page_header += '           <p class="medium-small grey-text">Type</p>';
    listing_page_header += '       </div>';
    listing_page_header += '     <div class="col s6 m2 l2 center-align">';
    listing_page_header += '         <h4 class="card-title grey-text text-darken-4">' + details.accomodates + '</h4>';
    listing_page_header += '        <p class="medium-small grey-text">Accomodates</p>';
    listing_page_header += '      </div>';
    listing_page_header += '       <div class="col s6 m2 l2 center-align">';
    listing_page_header += '           <h4 class="card-title grey-text text-darken-4">' + details.bedrooms + '</h4>';
    listing_page_header += '           <p class="medium-small grey-text">Bedroom(s)</p>';
    listing_page_header += '       </div>';
    listing_page_header += '       <div class="col s6 m2 l2 center-align">';
    listing_page_header += '           <h4 class="card-title grey-text text-darken-4">' + details.beds + '</h4>';
    listing_page_header += '           <p class="medium-small grey-text">Bed(s)</p>';
    listing_page_header += '        </div>';
    listing_page_header += '    </div>';
    listing_page_header += '  </div>';
    listing_page_header += '  </div>';

    $("#listing-page-header").replaceWith(listing_page_header);


    var specifications = "";
    specifications += '<div id="specifications-row" class="row">';
    specifications += '                     <div class="col s4 no-padding grey-text">';
    specifications += '                         Specifications';
    specifications += '                       </div>';
    specifications += '                       <div class="col s4">';
    specifications += '                           <p>Accomodates: <b id="accomodates">' + details.accomodates + '</b></p>';
//    specifications += '                            <p>Bathrooms: <b id="bathrooms">' + details.bathrooms + '</b></p>';
    specifications += '                            <p>Bedrooms: <b id="bedrooms">' + details.bedrooms + '</b></p>';
    specifications += '                           <p>Beds: <b id="beds">' + details.beds + '</b></p>';
    specifications += '                       </div>';
    specifications += '                       <div class="col s4">';
    specifications += '                           <p>Check-In: <b id="check-in">' + details.available_from + '</b></p>';
//    specifications += '                          <p>Pets Allowed: <b id="pets-allowed">None</b></p>';
    specifications += '                         <p>Property Type: <b id="property-type">' + details.type + '</b></p>';
    specifications += '                        <p>Room Type: <b id="room-type">' + details.room_type.replace(/\+/g, " ") + '</b></p>';
    specifications += '                    </div>';
    specifications += '               </div>';

    $("#specifications-row").replaceWith(specifications);



    var amenities = "";
    amenities += '<div id="amenities-row" class="row">';
    amenities += '                              <div class="col s4 no-padding grey-text">';
    amenities += '                                    Amenities';
    amenities += '                                 </div>';
    amenities += '                                 <div id="amenities-left" class="col s4">';
    amenities += '';
    amenities += '                               </div>';
    amenities += '                               <div id="amenities-right" class="col s4">';
    amenities += '                                    <a href="#">View All</a>';
    amenities += '                                </div>';
    amenities += '                            </div>';

    $("#amenities-row").replaceWith(amenities);


    var prices = "";

    prices += '<div id="prices-row" class="row">';
//          prices += '                          <div class="col s4 no-padding grey-text">';
//          prices += '                              Prices';
//         prices += '                           </div>';
//          prices += '                          <div class="col s4">';
//          prices += '                              <p>Extra People: <b>$<span id="extra-people">0</span> per night</b></p>';
//          prices += '                              <p>Cleaning Fee: <b>$<span id="cleaning-fee">0</span></b></p>';
//          prices += '                          </div>';
//          prices += '                          <div class="col s4">';
//          prices += '                              <p>Weekly Discount: <b><span id="weekly-discount">0</span>%</b></p>';
//           prices += '                             <p>Monthly Discount: <b><span id="monthly-discount">0</span>%</b></p>;'
//          prices += '                              <p>Cancellation: <b id="cancellation">Long Term/Short term</b></p>';
//          prices += '                          </div>';
    prices += '                      </div>';


    $("#prices-row").replaceWith(prices);



    var description = "";


    description += '         <p id="about-this-space">' + details.description.replace(/\+/g, " ") + '</p>';
    description += '               <br>';


    $("#about-this-space").replaceWith(description);

});

function callAjaxSuccessful() {

}

function errorFunction() {

}

function get_listing(id) {
    var url = "action.php";

    var obj = syncAjax(url, {cmd: 5, pid: id});

    if (obj.result === 0) {
        alert(obj.message);
    }
    // paste from search listings function
    else if (obj.result === 1) {
        alert("success");
    }
    else if (obj.result === 2) {
        Materialize.toast(obj.message, 3000);

        $("#listings").replaceWith(card);
    }
}