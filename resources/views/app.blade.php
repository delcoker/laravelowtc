<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="description" content="One With The City. Travel the world and live like a local.">
        <meta name="keywords" content="owtc, one with the city, accra, ghana, airbnb,">
        <title>{{ $title }}</title>

        <!-- Favicons-->
        <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-office.jpgtouch-icon-152x152.png">
        <!-- For iPhone -->
        <meta name="msapplication-TileColor" content="#00bcd4">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
        <!-- For Windows Phone -->


        <!-- CORE CSS-->
        <link href="{{ URL::asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="{{ URL::asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- CUSTOM CSS -->
        @yield('page_css')
    </head>
    <body class="blue-grey">

        @yield('header')

        @yield('content') 

        @yield('footer')


        <footer class="page-footer blue-grey">
            <div class="footer-copyright">
                Made by<a class="blue-grey-text text-lighten-3" href="http://www.twitter.com/niiapa">blankieGH</a> & <a class="blue-grey-text text-lighten-3" href="http://www.twitter.com/niiapa">deLoop</a>
            </div>
        </footer>

        <script src="{{ URL::asset('js/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ URL::asset('js/materialize.js') }}"></script>
        <script src="{{ URL::asset('js/across_all.js') }}" type="text/javascript"></script>
        @yield('footer_script')
    </body>
</html>