@extends ('app')

@section ('page_css')
<link href="{{ URL::asset('css/nouislider.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('css/listings.css') }}" type="text/css" rel="stylesheet">

@stop 

@section('header')
<header id="header">
    <!-- Fixed Navbar -->
    <div class="navbar-fixed">
        <!-- page header nav -->
        <nav class="blue-grey">
            <div class="nav-wrapper container">
                <h1 class="logo-wrapper"><a href="homepage.html" class="brand-logo darken-1"><img src="images/materialize-logo.png" alt="One With The City Logo"></a> <span class="logo-text">One With The City</span></h1>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="host.html" class="waves-effect waves-light btn">Become A Host</a>
                    </li>
                    <li>
                        <a href="help.html">Help</a>
                    </li>
                    <li>
                        <a href="signup.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                </ul>
                <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="material-icons">menu</i></a>
                <ul class="side-nav" id="mobile-menu">
                    <li>
                        <a href="host.html">Become a Host</a>
                    </li>
                    <li>
                        <a href="help.html">Help</a>
                    </li>
                    <li>
                        <a href="signup.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- end header nav -->
    </div>
    <!-- end fixed-navbar -->
</header>
@stop

@section('content')
<main>
    <!-- START CONTROL PANEL -->
    <div id="control-panel" class="container">
        <div class="row">
            <div class="col s10 offset-s1 m8 offset-m2 l8 offset-l2 card">
                <div class="container">
                    <div class="row">
                        <form id="check-in-form">
                            <div class="col s12 m6 l3">
                                <div class="input-field">
                                    <i class="material-icons prefix tooltipped" data-position="top" data-delay="50" data-tooltip="Date of Arrival">today</i>
                                    <input id="check-in" type="date" class="datepicker">
                                    <label for="check-in" class="center-align">Check In</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="input-field">
                                    <i class="material-icons prefix tooltipped" data-position="top" data-delay="50" data-tooltip="Date of Departure">today</i>
                                    <input id="check-out" type="date" class="datepicker">
                                    <label for="check-out" class="center-align">Check Out</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="input-field">
                                    <select id="guests" onchange="search_listings(1)">
                                        <option value="" disabled select>0 Guests</option>
                                        <option value="1" >1 Guest</option>
                                        <option value="2">2 Guests</option>
                                        <option value="3">3 Guests</option>
                                        <option value="4">4 Guests</option>
                                        <option value="5">5 Guests</option>
                                        <option value="6">6 Guests</option>
                                        <option value="7">7 Guests</option>
                                        <option value="8">8 Guests</option>
                                        <option value="9">9 Guests</option>
                                        <option value="10">10 Guests</option>
                                        <option value="11">11 Guests</option>
                                        <option value="12">12 Guests</option>
                                        <option value="13">13 Guests</option>
                                        <option value="14">14 Guests</option>
                                        <option value="15">15 Guests</option>
                                        <option value="16+">16+ Guests</option>
                                    </select>
                                    <label for="guests" class="tooltipped" data-position="top" data-delay="50" data-tooltip="Number of Guests">Guests</label>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="input-field">
                                    <select id="children" onchange="search_listings(1)">
                                        <option value="0">0 Children</option>
                                        <option value="1">1 Child</option>
                                        <option value="2">2 Children</option>
                                        <option value="3">3 Children</option>
                                        <option value="4">4 Children</option>
                                        <option value="5">5 Children</option>
                                        <option value="6">6 Children</option>
                                        <option value="7">7 Children</option>
                                        <option value="8">8 Children</option>
                                        <option value="9">9 Children</option>
                                        <option value="10">10 Children</option>
                                        <option value="11">11 Children</option>
                                        <option value="12">12 Children</option>
                                        <option value="13">13 Children</option>
                                        <option value="14">14 Children</option>
                                        <option value="15">15 Children</option>
                                        <option value="16+">16+ Children</option>
                                    </select>
                                    <label for="children" class="tooltipped" data-position="top" data-delay="50" data-tooltip="Number of Children">Children</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- ./row -->
                </div>
                <!-- ./container -->
                <div class="container">
                    <div class="divider"></div>
                </div>
                <br>
                <div class="container" style="padding: 0 10px 0 10px;">
                    <div class="row">
                        <form action="#" id="room-type-form">
                            <div class="col s12 m4 l4">
                                <input type="checkbox" class="filled-in checkbox" id="entire-home-apt" onchange="search_listings(1)"/>
                                <label for="entire-home-apt">Entire Home/Apt</label>
                            </div>
                            <div class="col s12 m4 l4 center">
                                <input type="checkbox" class="filled-in checkbox" id="private-room" onchange="search_listings(1)"/>
                                <label for="private-room">Private Room</label>
                            </div>
                            <div class="col s12 m4 l4" align="right">
                                <input type="checkbox" class="filled-in checkbox" id="shared-room" onchange="search_listings(1)"/>
                                <label for="shared-room">Shared Room</label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container">
                    <div class="divider"></div>
                </div>
                <br>
                <div class="container">
                    <div class="row" style="padding: 0 15px 0 15px;">
                        <div id="price-range-slider"></div>
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col s6 m6 l6" align="left">
                            <div id="min-price" onchange="search_listings(1)">Minimum</div>
                        </div>
                        <div class="col s6 m6 l6" align="right">
                            <div id="max-price" onchange="search_listings(1)">Maximum</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./row -->
    </div>
    <!-- ./container -->
    <!-- END CONTROL PANEL -->

    <!-- LISTINGS -->
    <div id="listings">
        <div class="row">
            <div class="col s12 m10 offset-m1 l10 offset-l1">


                <div class="col s12 m6 l4">
                    <div id="profile-card" class="card">
                        <div class="card-image waves-effect waves-block waves-light" onclick="get_listing()">
                            <img class="activator" src="images/bg.jpg" alt="Listing Image">
                        </div>
                        <div class="card-content">
                            <img src="images/avatar.jpg" alt="User Avatar" class="circle responsive-img activator card-profile-image">
                            <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
                                <i class="material-icons">shopping_cart</i>
                            </a>

                            <span class="card-title activator grey-text text-darken 4">Random Building</span>
                            <p>$250</p>
                            <p><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star</i><i class="material-icons">star_half</i></p>
                        </div>
                    </div>
                </div>


                <!--more goes here-->

            </div>
        </div>

    </div>
    <!-- END LISTINGS -->

    <!-- START PAGINATION -->
    <div id="pagination" class="container center-align">
        <ul class="pagination">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><div id = "one" >1</div></li>
            <!--                    <li class="waves-effect"><a href="#!">2</a></li>
                                <li class="waves-effect"><a href="#!">3</a></li>
                                <li class="waves-effect"><a href="#!">4</a></li>
                                <li class="waves-effect"><a href="#!">5</a></li>-->
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>
    </div>
    <!-- END PAGINATION -->




    <!--             START PAGINATION 
                <div id="pagination" class="container center-align">
                    <ul class="pagination">
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                        <li class="active"><a href="#!">1</a></li>
                        <li class="waves-effect"><a href="#!">2</a></li>
                        <li class="waves-effect"><a href="#!">3</a></li>
                        <li class="waves-effect"><a href="#!">4</a></li>
                        <li class="waves-effect"><a href="#!">5</a></li>
                        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    </ul>
                </div>-->
    <!-- END PAGINATION -->

</main>
@stop

@section('footer_script')
<script src="{{ URL::asset('js/nouislider.js') }}"></script>
<script type="text/javascript">
                            $(document).ready(function () {
                                $(".button-collapse").sideNav({
                                    edge: 'right'
                                });


                                $('select').material_select();


                                $('.datepicker').pickadate({
                                    selectMonths: true, // Creates a dropdown to control month
                                    selectYears: 15 // Creates a dropdown of 15 years to control year
                                });


                                $('.filled-in.checkbox').click(function () {
                                    $('.checkbox').each(function () {
                                        $(this).prop('checked', false);
                                    });
                                    $(this).prop('checked', true);
                                });

                                initializeSlider();
                            });

//                                                $(window).load(function () {
//
//                                                });

                            function initializeSlider() {
                                var slider = document.getElementById("price-range-slider");

                                noUiSlider.create(slider, {
                                    start: [0, 10000]
                                    , connect: true
                                    , step: 10
                                    , range: {
                                        'min': 0
                                        , 'max': 10000
                                    }
                                    , format: wNumb({
                                        decimals: 0
                                    })
                                });

                                var minPrice = document.getElementById('min-price');
                                var maxPrice = document.getElementById('max-price');

                                slider.noUiSlider.on('update', function (values, handle) {

                                    if (handle) {
//                                                            search_listings(1);
                                        maxPrice.innerHTML = "$" + values[handle];
                                    } else {
                                        minPrice.innerHTML = "$" + values[handle];
                                    }
                                });

                                slider.noUiSlider.on('change', function () {
                                    search_listings(1);
                                });


                            }
</script>
<script src="{{ URL::asset('js/del.js') }}" type="text/javascript"></script>
@stop