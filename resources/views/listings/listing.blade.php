@extends ('app')

@section ('page_css')
<link href="{{ URL::asset('css/nouislider.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('css/listing.css') }}" type="text/css" rel="stylesheet">

@stop 

@section ('header')
<header id="header">
    <!-- Fixed Navbar -->
    <div class="navbar">
        <!-- page header nav -->
        <nav class="blue-grey">
            <div class="nav-wrapper container">
                <h1 class="logo-wrapper">
                    <a href="homepage.html" class="brand-logo darken-1">
                        <img src="{{ URL::asset('images/materialize-logo.png') }}" alt="One With The City Logo">
                    </a>
                    <span class="logo-text">One With The City</span>
                </h1>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="help.html">Help</a>
                    </li>
                    <li>
                        <a href="signup.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                </ul>
                <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="material-icons">menu</i></a>
                <ul class="side-nav" id="mobile-menu">
                    <li>
                        <a href="help.html">Help</a>
                    </li>
                    <li>
                        <a href="signup.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- end header nav -->
    </div>
    <!-- end fixed-navbar -->
</header>
@stop

@section('content')
<main>
    <!-- START LISTING HEADER -->
    <div class="custom-container">
        <div id="listing-page-header" class="card">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="{{ URL::asset('images/bg.jpg') }}" alt="user background">
            </div>
            <div class="card-profile-image">
                <img src="{{ URL::asset('images/avatar.jpg') }}" alt="profile image" class="circle responsive-img activator">
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col s12 m2 offset-m2 l2 offset-l2 center-align">
                        <h4 class="card-title grey-text text-darken-4">Roger Waters</h4>
                        <p class="medium-small grey-text">Host</p>
                    </div>
                    <div class="col s6 m2 l2 center-align">
                        <h4 class="card-title grey-text text-darken-4">Apartment</h4>
                        <p class="medium-small grey-text">Type</p>
                    </div>
                    <div class="col s6 m2 l2 center-align">
                        <h4 class="card-title grey-text text-darken-4">10+</h4>
                        <p class="medium-small grey-text">Accomodates</p>
                    </div>
                    <div class="col s6 m2 l2 center-align">
                        <h4 class="card-title grey-text text-darken-4">0</h4>
                        <p class="medium-small grey-text">Bedrooms</p>
                    </div>
                    <div class="col s6 m2 l2 center-align">
                        <h4 class="card-title grey-text text-darken-4">0</h4>
                        <p class="medium-small grey-text">Beds</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END LISTING HEADER -->

    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->

    <!-- START LISTING DETAILS -->
    <div class="custom-container">
        <!-- START ROW -->
        <div class="row">
            <!-- START LEFT SIDE -->
            <div id="listing-details" class="col s12 m12 l9">
                <div class="card z-depth-1">
                    <div class="card-title blue-grey">
                        <div class="container">
                            About this listing
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <p>Short About goes here</p>
                        </div>

                        <div class="divider"></div>
                        <br>

                        <div id="specifications-row" class="row">
                            <div class="col s4 no-padding grey-text">
                                Specifications
                            </div>
                            <div class="col s4">
                                <p>Accomodates: <b id="accomodates">0</b></p>
                                <p>Bathrooms: <b id="bathrooms">0</b></p>
                                <p>Bedrooms: <b id="bedrooms">0</b></p>
                                <p>Beds: <b id="beds">0</b></p>
                            </div>
                            <div class="col s4">
                                <p>Check-In: <b id="check-in">00:00</b></p>
                                <p>Pets Allowed: <b id="pets-allowed">None</b></p>
                                <p>Property Type: <b id="property-type">Apartment</b></p>
                                <p>Room Type: <b id="room-type">Entire home/apt</b></p>
                            </div>
                        </div>

                        <div class="divider"></div>
                        <br>

                        <div id="amenities-row" class="row">
                            <div class="col s4 no-padding grey-text">
                                Amenities
                            </div>
                            <div id="amenities-left" class="col s4">

                            </div>
                            <div id="amenities-right" class="col s4">
                                <a href="#">View All</a>
                            </div>
                        </div>

                        <!--                                <div class="divider"></div>
                                                        <br>-->

                        <div id="prices-row" class="row">
                            <div class="col s4 no-padding grey-text">
                                Prices
                            </div>
                            <div class="col s4">
                                <p>Extra People: <b>$<span id="extra-people">0</span> per night</b></p>
                                <p>Cleaning Fee: <b>$<span id="cleaning-fee">0</span></b></p>
                            </div>
                            <div class="col s4">
                                <p>Weekly Discount: <b><span id="weekly-discount">0</span>%</b></p>
                                <p>Monthly Discount: <b><span id="monthly-discount">0</span>%</b></p>
                                <p>Cancellation: <b id="cancellation">Long Term/Short term</b></p>
                            </div>
                        </div>

                        <div class="divider"></div>
                        <br>

                        <div id="description-row" class="row">
                            <div class="col s4 no-padding grey-text">
                                Description
                            </div>
                            <div class="col s8">
                                <p><b>About This Space</b></p>
                                <p id="about-this-space">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <br>
<!--                                        <p><b>Guest Access</b></p>
                                <p id="guest-access">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <br>
                                <p><b>Interaction with Guests</b></p>
                                <p id="interaction-with-guests">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <br>
                                <p><b>The Neighborhood</b></p>
                                <p id="the-neighborhood">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <br>
                                <p><b>Getting Around</b></p>
                                <p id="getting-around">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <br>
                                <p><b>Other Things To Note</b></p>
                                <p id="other-things-to-note">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>-->
                            </div>
                        </div>

                        <!--                                <div class="divider"></div>
                                                        <br>
                        
                                                        <div id="house-rules-row" class="row">
                                                            <div class="col s4 no-padding grey-text">
                                                                House Rules
                                                            </div>
                                                            <div class="col s8">
                                                                <p id="house-rules">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                            </div>
                                                        </div>
                        
                                                        <div class="divider"></div>
                                                        <br>
                        
                                                        <div id="safety-features-row" class="row">
                                                            <div class="col s4 no-padding grey-text">
                                                                Safety Features
                                                            </div>
                                                            <div id="safety-features-left" class="col s4">
                                                                <p>Fire Extinguisher</p>
                                                            </div>
                                                            <div id="safety-features-right" class="col s4">
                        
                                                            </div>
                                                        </div>-->

                        <div class="divider"></div>
                        <br>

                        <div id="availability-row" class="row">
                            <div class="col s4 no-padding grey-text">
                                Availability
                            </div>
                            <div id="safety-features-left" class="col s4">
                                <p><b><span id="minimum-nights">1</span> night</b> minimum stay</p>
                            </div>
                            <div id="safety-features-right" class="col s4">
                                <a href="#">View Calendar</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END LEFT SIDE -->
            <!-- START RIGHT SIDE -->
            <div id="listing-booking" class="col s12 m12 l3">
                <div id="listing-booking-card" class="card z-depth-1">
                    <div class="card-title blue-grey darken-2">
                        <div class="center-align">
                            $Price Per Month
                        </div>
                    </div>
                    <div class="card-content" style="padding-bottom: 5px;">
                        <div class="row">
                            <form>
                                <div class="col s12">
                                    <div class="input-field">
                                        <i class="material-icons prefix tooltipped" data-position="left" data-delay="50" data-tooltip="Date of Arrival">today</i>
                                        <input id="check-in" type="date" name="check-in" class="datepicker">
                                        <input id='check-in' type="date" name="check-in_hidden" class="datepicker" hidden>
                                        <label for="check-in" class="center-align">Check In</label>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="input-field">
                                        <i class="material-icons prefix tooltipped" data-position="left" data-delay="50" data-tooltip="Date of Arrival">today</i>
                                        <input id="check-out" type="date" class="datepicker">
                                        <label for="check-out" class="center-align">Check Out</label>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="input-field">
                                        <select id="guests">
                                            <option value="" disabled select>0 Guests</option>
                                            <option value="1">1 Guest</option>
                                            <option value="2">2 Guests</option>
                                            <option value="3">3 Guests</option>
                                            <option value="4">4 Guests</option>
                                            <option value="5">5 Guests</option>
                                            <option value="6">6 Guests</option>
                                            <option value="7">7 Guests</option>
                                            <option value="8">8 Guests</option>
                                            <option value="9">9 Guests</option>
                                            <option value="10">10 Guests</option>
                                            <option value="11">11 Guests</option>
                                            <option value="12">12 Guests</option>
                                            <option value="13">13 Guests</option>
                                            <option value="14">14 Guests</option>
                                            <option value="15">15 Guests</option>
                                            <option value="16+">16+ Guests</option>
                                        </select>
                                        <label for="guests" class="tooltipped" data-position="top" data-delay="50" data-tooltip="Number of Guests">Guests</label>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="input-field">
                                        <select id="children">
                                            <option value="0">0 Children</option>
                                            <option value="1">1 Child</option>
                                            <option value="2">2 Children</option>
                                            <option value="3">3 Children</option>
                                            <option value="4">4 Children</option>
                                            <option value="5">5 Children</option>
                                            <option value="6">6 Children</option>
                                            <option value="7">7 Children</option>
                                            <option value="8">8 Children</option>
                                            <option value="9">9 Children</option>
                                            <option value="10">10 Children</option>
                                            <option value="11">11 Children</option>
                                            <option value="12">12 Children</option>
                                            <option value="13">13 Children</option>
                                            <option value="14">14 Children</option>
                                            <option value="15">15 Children</option>
                                            <option value="16+">16+ Children</option>
                                        </select>
                                        <label for="children" class="tooltipped" data-position="top" data-delay="50" data-tooltip="Number of Children">Children</label>
                                    </div>
                                </div>

                                <!-- ACCOMODATION COSTS -->
                                <div id="accomodation-costs" hidden>
                                    <div class="col s8 left-align">
                                        Accomodation
                                    </div>
                                    <div id="accomodation-cost" class="col s4 right-align">
                                        $0
                                    </div>

                                    <div class="col s12">
                                        <div class="divider"></div>
                                    </div>

                                    <div class="col s8 left-align">
                                        Monthly Discount
                                    </div>
                                    <div id="monthly-discount" class="col s4 right-align">
                                        -$0
                                    </div>

                                    <div class="col s12">
                                        <div class="divider"></div>
                                    </div>

                                    <div class="col s8 left-align">
                                        Cleaning Fee
                                    </div>
                                    <div id="cleaning-fee" class="col s4 right-align">
                                        $0
                                    </div>

                                    <div class="col s12">
                                        <div class="divider"></div>
                                    </div>

                                    <div class="col s8 left-align">
                                        Service Fee
                                    </div>
                                    <div id="service-fee" class="col s4 right-align">
                                        $0
                                    </div>

                                    <div class="col s12">
                                        <div class="divider"></div>
                                    </div>

                                    <div class="col s8 left-align">
                                        Total
                                    </div>
                                    <div id="total" class="col s4 right-align">
                                        $0
                                    </div>
                                </div>
                                <!-- END ACCOMODATION COSTS -->

                                <div class="col s12 center-align" style="margin-top: 20px;">
                                    <a href="#" id="request-booking" class="btn waves effect waves-light" style="width: 100%;">Request Booking</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END RIGHT SIDE -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END LISTING DETAILS -->
</main>

@stop

@section('footer_script')
<script src="{{ URL::asset('js/del_listing.js') }}" type="text/javascript"></script>
@stop