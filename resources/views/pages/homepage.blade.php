@extends ('app')

@section ('page_css')

<link href="{{ URL::asset('css/homepage.css') }}" type="text/css" rel="stylesheet">

@stop 

@section('header')
<header id="header">
    <!-- Fixed Navbar -->
    <div class="navbar-fixed">
        <!-- page header nav -->
        <nav class="blue-grey">
            <div class="nav-wrapper container">
                <h1 class="logo-wrapper"><a href="homepage.html" class="brand-logo darken-1"><img src="images/materialize-logo.png" alt="One With The City Logo"></a> <span class="logo-text">One With The City</span></h1>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="host.html" class="waves-effect waves-light btn">Become A Host</a>
                    </li>
                    <li>
                        <a href="help.html">Help</a>
                    </li>
                    <li>
                        <a href="signup.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                </ul>
                <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="material-icons">menu</i></a>
                <ul class="side-nav" id="mobile-menu">
                    <li>
                        <a href="host.html">Become a Host</a>
                    </li>
                    <li>
                        <a href="help.html">Help</a>
                    </li>
                    <li>
                        <a href="signup.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="login.html">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- end header nav -->
    </div>
    <!-- end fixed-navbar -->
</header>
@stop

@section('content')
<main class="valign-wrapper">
    <!-- START CONTAINER -->
    <div class="container valign">
        <div class="row">
            <div class="col s10 offset-s1 m8 offset-m2 l6 offset-l3 z-depth-2 card no-padding grey lighten-4">
                <div class="card-title blue-grey">
                    <p class="container center">Find a place to stay</p>
                </div>
                <div class="card-content">
                    <form id="search-form">
                        <div class="row margin">
                            <div class="input-field col s10 offset-s1 m10 offset-m1 l10 offset-l1">
                                <i class="material-icons prefix tooltipped" data-position="left" data-delay="50" data-tooltip="Date of Arrival">today</i>
                                <input id="check-in" type="date" name="check-in" class="datepicker">
                                <input id='check-in' type="date" name="check-in_hidden" class="datepicker" hidden>
                                <label for="check-in" class="center-align">Check In</label>
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s10 offset-s1 m10 offset-m1 l10 offset-l1">
                                <i class="material-icons prefix tooltipped" data-position="left" data-delay="50" data-tooltip="Date of Departure">today</i>
                                <input id="check-out" type="date" name="check-out" class="datepicker">
                                <input id='check-out' type="date" name="check-out_hidden" class="datepicker" hidden>
                                <label for="check-out" class="center-align">Check Out</label>
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s10 offset-s1 m10 offset-m1 l4 offset-l2">
                                <select id="guests">
                                    <option value="" disabled select>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16+">16+</option>
                                </select>
                                <label for="guests" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Number of Guests">Guests</label>
                            </div>
                            <div class="input-field col s10 offset-s1 m10 offset-m1 l4 ">
                                <select id="children">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16+">16+</option>
                                </select>
                                <label for="children" class="tooltipped" data-position="right" data-delay="50" data-tooltip="Number of Children">Children</label>
                            </div>
                        </div>
                        <br>
                        <div class="row center">
                            <div class="input-field">
                                <button class="btn-large waves effect waves-light" type="submit" name="submit">Live Like a Local</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <!-- ./col s12 m10 -->
        </div>
        <!-- ./row -->
    </div>
    <!-- END CONTAINER -->
</main>
@stop

@section('footer_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".button-collapse").sideNav({
                edge: 'right'
            });

            $('select').material_select();
        });

        $(window).load(function () {

        });

    </script>
    <script src="{{ URL::asset('js/del.js') }}" type="text/javascript"></script>
@stop