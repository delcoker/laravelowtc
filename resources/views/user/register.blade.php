@extends ('app')

@section ('page_css')

<link href="{{ URL::asset('css/login.css') }}" type="text/css" rel="stylesheet">

@stop 

@section ('header')
<header id="header">
    <!-- page header nav -->
    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <h1 class="logo-wrapper"><a href="homepage.html" class="brand-logo darken-1"><img src="images/materialize-logo.png" alt="One With The City Logo"></a> <span class="logo-text">One With The City</span></h1>
            <ul class="right hide-on-med-and-down">
                <li>
                    <a href="host.html" class="waves-effect waves-light btn">Become A Host</a>
                </li>
                <li>
                    <a href="help.html">Help</a>
                </li>
                <li>
                    <a href="page-register.html">Sign Up</a>
                </li>
                <li>
                    <a href="page-login.html">Login</a>
                </li>
            </ul>
            <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="material-icons">menu</i></a>
            <ul class="side-nav" id="mobile-menu">
                <li>
                    <a href="host.html">Become a Host</a>
                </li>
                <li>
                    <a href="help.html">Help</a>
                </li>
                <li>
                    <a href="page-register.html">Sign Up</a>
                </li>
                <li>
                    <a href="page-login.html">Login</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- end header nav -->
</header>
@stop 

@section ('content')
<main class="valign-wrapper">
    <!-- START CONTAINER -->
    <div class="container valign">
        <div class="row">
            <div class="col s10 offset-s1 m8 offset-m2 l6 offset-l3 z-depth-2 card no-padding grey lighten-4">
                <div class="card-title blue-grey">
                    <p class="container center">Register</p>
                </div>
                <!--<div class="card-panel">-->
                <div class="card-content">
                    <div class="container">
                        <div class="row">

                            {!! Form::open(['url' => 'listings']) !!}
                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="mdi-action-account-circle prefix"></i>

                                    {!! Form::text('first_name', null, ['class' => 'validate', 'required', 'id' => 'first_name']) !!}
                                    {!! Form::label('first_name', 'First Name:') !!}
                                </div>

                                <div class="input-field col s6">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    {!! Form::text('last_name', null, ['class' => 'validate', 'required']) !!}
                                    {!! Form::label('last_name', 'Last Name:') !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="mdi-action-account-circle prefix"></i>
                                    {!! Form::text('other_names', null, ['class' => 'validate']) !!}
                                    {!! Form::label('other_names', 'Other Names:') !!}
                                </div>
                                <div class="input-field col s6">
                                    <i class="mdi-communication-email prefix"></i>
                                    {!! Form::text('email', null, ['class' => 'validate', 'required']) !!}
                                    {!! Form::label('email', 'Email:') !!}
                                </div>

                            </div>

                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="mdi-action-lock-outline prefix"></i>
                                    {!! Form::password('password', null, ['class' => 'validate', 'required']) !!}
                                    {!! Form::label('password', 'Password:') !!}
                                </div>


                                <div class="input-field col s6">
                                    <i class="mdi-action-lock-outline prefix"></i>
                                    {!! Form::password('confirm_password', null, ['class' => 'validate', 'required']) !!}
                                    {!! Form::label('confirm_password', 'Confirm Password:') !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="material-icons prefix tooltipped" data-position="left" data-delay="50" data-tooltip="Primary Phone">phone</i>
                                    {!! Form::text('primary_phone', null, ['class' => 'validate', 'required']) !!}
                                    {!! Form::label('primary_phone', 'Primary Phone:') !!}
                                </div>

                                <div class="input-field col s6">
                                    <i class="material-icons prefix tooltipped" data-position="left" data-delay="50" data-tooltip="Seondary Phone Number">phone</i>
                                    {!! Form::text('secondary_phone', null, ['class' => 'validate']) !!}
                                    {!! Form::label('secondary_phone', 'Secondary Phone:') !!}
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="material-icons tooltipped prefix" data-position="left" data-delay="50" data-tooltip="Date of Birth">perm_contact_calendar</i>
                                    {!! Form::date('dob', null, ['class' => 'datepicker validate']) !!}
                                    {!! Form::date('dob_hidden', null, ['class' => 'datepicker validate' , 'hidden', 'id'=>'dob']) !!}
                                    <!--<input id='dob' type="date" name="dob_hidden" class="datepicker validate" hidden>-->
                                    {!! Form::label('dob', 'Date of Birth:') !!}
                                </div>

                                <div class="file-field input-field col s6">
                                    <div class="btn">
                                        <span>Image</span>
                                        {!! Form::file('myFile', null) !!}
                                    </div>
                                    <div class="file-path-wrapper">
                                        {!! Form::text('image_path', null, ['class' => 'file-path validate']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons tooltipped prefix" data-position="left" data-delay="50" data-tooltip="Address">home</i>
                                    {!! Form::textarea('postal_address', null, ['class' => 'materialize-textarea']) !!}
                                    {!! Form::label('postal_address', 'Postal Address:') !!}
                                </div>
                            </div>

                            <div class="row">

                                <div class="row">
                                    <div class="input-field col s12" align="center">
                                        {!! Form::button('Register',  ['class' => 'btn waves-effect waves-light', 'type'=> 'submit']) !!}

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6 m6 l6">
                                    <p class="margin medium-small"><a href="{{ URL::asset('login.html') }}">Already Registered?</a></p>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                    <p class="margin right-align medium-small"><a href="forgot-password.html">Forgot password ?</a></p>
                                </div>
                            </div>

                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
</main>
@stop

@section ('footer_script')
<script type="text/javascript">
    $(document).ready(function () {
        $(".button-collapse").sideNav({
            edge: 'right'
        });


        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year
            formatSubmit: 'yyyy-mm-dd',
            hiddenSuffix: '_hidden',
            hiddenName: true
        });
    });

</script>
@stop
