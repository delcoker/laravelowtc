@extends ('app')

@section ('page_css')

<link href="{{ URL::asset('css/login.css') }}" type="text/css" rel="stylesheet">

@stop 

@section ('header')
<header id="header">
    <!-- page header nav -->
    <nav class="blue-grey">
        <div class="nav-wrapper container">
            <h1 class="logo-wrapper"><a href="homepage.html" class="brand-logo darken-1"><img src="images/materialize-logo.png" alt="One With The City Logo"></a> <span class="logo-text">One With The City</span></h1>
            <ul class="right hide-on-med-and-down">
                <li>
                    <a href="host.html" class="waves-effect waves-light btn">Become A Host</a>
                </li>
                <li>
                    <a href="help.html">Help</a>
                </li>
                <li>
                    <a href="page-register.html">Sign Up</a>
                </li>
                <li>
                    <a href="page-login.html">Login</a>
                </li>
            </ul>
            <a href="#" data-activates="mobile-menu" class="button-collapse right"><i class="material-icons">menu</i></a>
            <ul class="side-nav" id="mobile-menu">
                <li>
                    <a href="host.html">Become a Host</a>
                </li>
                <li>
                    <a href="help.html">Help</a>
                </li>
                <li>
                    <a href="page-register.html">Sign Up</a>
                </li>
                <li>
                    <a href="page-login.html">Login</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- end header nav -->
</header>
@stop 

@section('content')
<main class="valign-wrapper">
    <!-- START CONTAINER -->
    <div class="container valign">
        <div id="login-page" class="row">
            <div class="col s10 offset-s1 m6 offset-m3 l4 offset-l4 z-depth-2 card no-padding grey lighten-4 center">
                <div class="card-title blue-grey">
                    <p class="container center">Login</p>
                </div>
                <div class="container">
                    <!--<form action="login.php" method='post'>-->
                    <form id="login-form">

                        <div class="row">
                            <div class="input-field col s12" align="center">
                                <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s12">
                                <i class="mdi-social-person-outline prefix"></i>
                                <input id="email" name="email" type="text" class="validate">
                                <label for="email" class="center-align">Email</label>
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s12">
                                <i class="mdi-action-lock-outline prefix"></i>
                                <input id="password" name="password" type="password" class=" validate" required>
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="input-field col s12 m12 l12 right-align">
                                    <input class="filled-in checkbox validate" type="checkbox" id="remember-me" />
                                    <label for="remember-me">Remember me</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <!--<a href="#" class="btn waves-effect waves-light col s12" onclick="login()">Login</a>-->
                                <button type="submit" class="btn waves-effect waves-light col s12 m10 offset-m1 l6 offset-l3" value='Login'>Login</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6 l6">
                                <p class="margin medium-small"><a href="register.html">Register Now!</a></p>
                            </div>
                            <div class="input-field col s6 m6 l6">
                                <p class="margin right-align medium-small"><a href="forgot-password.html">Forgot password ?</a></p>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
</main>
@stop

@section('footer_script')

@stop