<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    // mapping to my own table called property -- Del
    protected $table = 'property';
    
    protected $fillable = [
        'city_id', 'user_id', 'name', 'type', 'room_type', 'accomodates', 'bedrooms', 'beds', 'washrooms','host_language', 'description', 'location', 'price_per_day', 'allows_children', 'available_from', 'available_until'
    ];
}
