<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'PagesController@homepage');

Route::get('register', 'UserController@index');

Route::post('listings', 'UserController@register');

Route::get('login', 'UserController@login');


Route::get('listings', 'ListingsController@index');

Route::get('listing/{id}', 'ListingsController@listing');

Route::get('contact', 'PagesController@contact');

Route::get('about', 'PagesController@about');



//Route::get('/', function () { // return view('pages.homepage'); //});