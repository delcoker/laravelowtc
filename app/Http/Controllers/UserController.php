<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Picture;
use Request;

class UserController extends Controller {

    public function index() {
        $title = 'OWTC - Register';

        return view('user.register', compact('title'));
    }

    public function register() {

        $picture = Request::get('image_path');

        $last_inserted_pic_id = Picture::create(['uri' => $picture])->id;

//        return $last_inserted_pic_id;


        $input = Request::all();
        $input['picture_id'] = $last_inserted_pic_id;
        $input['role_id'] = 3;
        $input['verified'] = false;
        $input['date_of_birth'] = $input['dob'];
        
        User::create($input);
        
        return redirect('login');
    }

    public function login() {
        $title = 'OWTC - Login';

        return view('user.login', compact('title'));
    }

}
