<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function homepage()
    {
        $title = 'OWTC - One With The City';
        return view('pages.homepage', compact('title'));
    }
    
    
    
    public function about()
    {
//        $name = 'Kingston Coker';
        
//        return view('pages.about')->with('name':$name);
        
        $first = 'Kingston';
        $last = 'Coker'; 
        
        
        return view('pages.about', compact('first', 'last'));
    }
    
    
    
    public function contact(){
        
        return view('pages.contact');
    }
   
}