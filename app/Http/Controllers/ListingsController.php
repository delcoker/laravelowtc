<?php

//does not work if it is up here .. it does not recognize Porperty
//use App\Property;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Http\Requests;

class ListingsController extends Controller {

    public function index() {
        $title = 'OWTC - Listings';

        $listings = Property::all();

//        return $listings;

        return view('listings.listings', compact('title', 'listings'));
    }

    public function listing($id) {
        $title = 'OWTC - Listing';

        $listing = Property::findOrFail($id);
        
//        dd($listings);

//        return $listings;

        return view('listings.listing', compact('title', 'listing'));
    }

}
