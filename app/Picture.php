<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    // mapping to my own table called property -- Del
    protected $table = 'picture';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uri', 'status'
    ];
}
