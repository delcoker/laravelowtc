<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'picture_id', 'first_name', 'last_name', 'other_names', 'email', 'password', 'role_id', 'verified', 'date_of_birth', 'status' , 'postal_address', 'primary_phone', 'secondary_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}