<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->boolean('wifi');
            $table->boolean('generator');
            $table->boolean('tv');
            $table->boolean('security');
            $table->boolean('kitchen');
            $table->boolean('internet');
            $table->boolean('essentials');
            $table->boolean('air_conditioner');
            $table->boolean('washing_machine');
            $table->boolean('dstv');
            $table->boolean('breakfast');
            $table->boolean('pets_allowed');
            $table->boolean('smoking_allowed');
            $table->boolean('wheelchair_accessible');
            $table->boolean('buzzer');
            $table->boolean('doorman');
            $table->boolean('pool');
            $table->boolean('hot_tub');
            $table->boolean('gym');
            $table->boolean('smoke_detector');
            $table->boolean('first_aid_kit');
            $table->boolean('fire_extinguisher');
            $table->boolean('twenty_four_hour_check_in');
            $table->boolean('fire_alarm');
            $table->enum('status', array('enabled', 'disabled', 'deleted'));            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amenities');
    }
}