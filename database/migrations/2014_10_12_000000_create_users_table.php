<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('picture_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('other_names');
            $table->text('postal_address');
            $table->string('primary_phone');
            $table->string('secondary_phone');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('role_id');
            $table->string('verified'); 
            $table->dateTime('date_of_birth'); 
            $table->rememberToken();
            $table->enum('status', array('enabled', 'disabled', 'deleted')); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}