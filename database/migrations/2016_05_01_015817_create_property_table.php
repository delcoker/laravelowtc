<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('user_id');
            $table->string('name');
            $table->enum('type', array('House', 'Bed & Breakfast', 'Loft'));
            $table->enum('room_type', array('Entire Home/Apt', 'Private Room', 'Shared Room'));
            $table->integer('accomodates');
            $table->integer('beds');
            $table->integer('bedrooms');
            $table->integer('washrooms');
            $table->enum('host_language', array('English', 'Francais', 'English'));
            $table->text('description');
            $table->string('location');
            $table->decimal('price_per_day');
            $table->boolean('allows_children');
            $table->date('available_from');
            $table->date('available_until');
            $table->enum('status', array('enabled', 'disabled', 'deleted')); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property');
    }
}